#!/usr/bin/env bash

echo "✨✨✨ Starting to bootstrap project... ✨✨✨"

project_name=$1
project_name_template="$project_name"_template
package_name=$(echo "$project_name" | tr '-' '_')

echo "📋 Generating repo from template... 📋"
git clone -q https://gitlab.com/Fricounet/templatoons.git $project_name_template

mv $project_name_template/python/template/ $project_name && rm -rf $project_name_template
cd $project_name

# Rename correctly everyhting
echo "♻️ Renaming everything... ♻️"
mv package_name $package_name
mv tests/test_package_name.py tests/test_$package_name.py

perl -v > /dev/null 2>&1
if [[ $? -eq 0 ]]; then
    AUTHOR_NAME=$(git config --global --get user.name)
    # Need to escape the @ for perl
    _EMAIL=$(git config --global --get user.email)
    AUTHOR_EMAIL=${_EMAIL/@/\\@}

    find . -type f -exec perl -i -pe"s/project_name/$project_name/g" {} \;
    find . -type f -exec perl -i -pe"s/AUTHOR_NAME/$AUTHOR_NAME/g" {} \;
    find . -type f -exec perl -i -pe"s/AUTHOR_EMAIL/$AUTHOR_EMAIL/g" {} \;
    find . -type f -exec perl -i -pe"s/package_name/$package_name/g" {} \;
    perl -i -pe"s/TODAY_DATE/$(date -Idate)/g" CHANGELOG.md
else
    echo "😢 perl was not found, you will have to rename the placeholders in the files yourself. 😢"
    exit 1
fi

echo "🎉🎉🎉 Python project bootstrapped successfully! 🎉🎉🎉"
exit 0
