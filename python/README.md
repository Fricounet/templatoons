# Python template

This directory contains a template to bootstrap a basic Python project using Poetry.

## Bootstrap project

### Prerequisites

To execute the bootstrap script, you need `git`, `curl`, `perl` and to have your git name and email set with `git config --global user.name "FIRST_NAME LAST_NAME"` and `git config --global user.email "MY_NAME@example.com"`.

### Usage

You can either,

Clone this repository and add the boostrap script somewhere in your path with `ln -sr ./python-bootstrap.sh ~/.local/bin/python-bootstrap`.
Then, create your project with `python-bootstrap <project-name>`

Or,

Directly curl the bootstrap script and create a project with `curl https://gitlab.com/Fricounet/templatoons/-/raw/main/python/python-bootstrap.sh | bash -s <project-name>`

## Author

[Baptiste G-C](mailto:baptiste.gc@fricou.net)
