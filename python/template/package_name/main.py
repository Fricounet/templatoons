"""Main module of the project.
"""

import logging


def main() -> None:
    """Entry point of the python project."""
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    )
    # Instantiate and use the logger in a new file by using the following line
    log = logging.getLogger(__name__)
    log.info("hello world")


if __name__ == "__main__":
    # Do not put anything else than the call to `main` here.
    # This code will only be executed if you do not use the poetry script.
    # It is here to allow people to use the package without the poetry script.
    main()
