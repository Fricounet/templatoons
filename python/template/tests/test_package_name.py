"""This module tests the core logic of the application.
"""

from package_name import __version__


def test_version() -> None:
    """Checks poetry version"""
    assert __version__ == "0.1.0"
