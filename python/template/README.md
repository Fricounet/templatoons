# project_name

## Project

### Installation

Make sure you have [poetry](https://python-poetry.org/) and Python3.10 installed (you can install it with [pyenv](https://github.com/pyenv/pyenv)).

Run `poetry install` to install all python dependencies.

Run `pre-commit install` to install the pre-commit hooks.

### Testing

Run `poetry run py.test` to test your project.

### Running the project

Run `poetry run` to execute your project from the [main entrypoint](package_name/main.py)

## Author

[@AUTHOR_NAME](mailto:AUTHOR_EMAIL)
