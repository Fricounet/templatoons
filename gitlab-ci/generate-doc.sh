#!/usr/bin/env bash

cd gitlab-ci 2> /dev/null

# Remove the auto-generated lines from the README for idempotence
MARKER_LINE=$(cat README.md | grep -in "The usage instruction for each template can be found in its file:" | cut -f1 -d:)
head -$(($MARKER_LINE + 1)) README.md > tmp.md && mv tmp.md README.md

# For each template file, parse its first line and add it to the README
for FILE in $(find . -name "*.gitlab-ci.yml" | sort)
do
    DOCUMENTATION=$(head -1 $FILE | sed -e 's/^# Use this template to //' -e 's/\.$//')
    echo "- [$(basename $FILE .gitlab-ci.yml)]($FILE): $DOCUMENTATION" >> README.md
done

echo "🎉🎉🎉 Finished generating doc in gitlab-ci README 🎉🎉🎉"
exit 0
