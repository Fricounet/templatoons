# Gitlab-ci templates

Contains a list of jobs and pipelines that can be used directly in you gitlab projects.
These templates try to follow the guidelines set by Gitlab in its [Development Guide](https://docs.gitlab.com/ee/development/cicd/templates.html).

## Usage

For all the following templates, having its name <name>:
To include the template in your gitlab-ci.yml, you can either copy it directly in your `gitlab-ci.yml` file or use the `include` keyword.
With the include keyword:

- if your project is hosted on gitlab.com, you can use:

```yaml
include:
  - project: "fricounet/templatoons"
    ref: main # or vX.X.X to use a specific version
    file:
      - "/gitlab-ci/<name>.gitlab-ci.yml"
```

- if your project is on a private instance, you can use:

```yaml
include:
  # Replace `main` by a specific version tag if you want.
  - remote: "https://gitlab.com/fricounet/templatoons/-/raw/main/gitlab-ci/<name>.gitlab-ci.yml"
```

The documentation below is automatically generated with the script [generate-doc](./generate-doc.sh). There is a pre commit hook at the root of the repo to run the script on commit (`pre-commit install`).
The usage instruction for each template can be found in its file:

- [docker](./docker.gitlab-ci.yml): lint Dockerfiles using Hadolint
- [gitlab](./gitlab.gitlab-ci.yml): create a new gitlab release when creating a version tag
- [poetry](./poetry.gitlab-ci.yml): test a Python application that uses poetry
