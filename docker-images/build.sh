#!/usr/bin/env bash

usage()
{
   # Display usage message
   echo "Build and push docker images. You need to be already logged in to the registry to be able to push."
   echo
   echo "Syntax: ./build.sh [-r]"
   echo "options:"
   echo " -h     Print this Help."
   echo " -b     Build only, do not push image."
   echo " -r     Specify a different image registry than 'fricounet'."
   echo
}

build_image () {
    IMAGE_REGISTRY="$REGISTRY/$IMAGE"
    echo "building image $IMAGE_REGISTRY:$VERSION"

    # Add latest tag if VÈRSION is at the top of the versions file
    [[ $VERSION == $(head -n 1 $IMAGE/versions) ]] && TAG_LATEST="-t $IMAGE_REGISTRY:latest"
    docker build $IMAGE -t $IMAGE_REGISTRY:$VERSION $TAG_LATEST --build-arg VERSION=$VERSION --cache-from $IMAGE_REGISTRY:latest
}

push_image () {
    IMAGE_REGISTRY="$REGISTRY/$IMAGE"
    echo "pushing image $IMAGE_REGISTRY:$VERSION"
    docker push $IMAGE_REGISTRY:$VERSION

    # Push latest if VÈRSION is at the top of the versions file
    [[ $VERSION == $(head -n 1 $IMAGE/versions) ]] && docker push $IMAGE_REGISTRY:latest
}

build_and_push_image () {
    VERSION=$1
    if [[ -z $VERSION ]];then
        for VERSION in $(cat $IMAGE/versions)
        do
            build_image && $BUILD_ONLY || push_image
        done
    else
        build_image && $BUILD_ONLY || push_image
    fi
}

REGISTRY="fricounet"
AVAILABLE_IMAGES="$(ls -d */ | cut -f1 -d'/')"
BUILD_ONLY=false

while getopts ":bhr:" option; do
   case $option in
      b) # only build image
         BUILD_ONLY=true
         shift 3;;
      h) # display usage message
         usage
         exit;;
      r) # registry name
         REGISTRY=$OPTARG
         shift 2;;
     \?) # invalid option
         echo "Error: Invalid option"
         usage
         exit 1;;
   esac
done

IMAGE=$1
IMAGE_VERSION=$2

ls $IMAGE > /dev/null
if [[ $? -ne 0 ]]; then
    echo "image name $IMAGE does not correspond to any available image"
    echo "Available images:"
    echo "$AVAILABLE_IMAGES"
    exit 1
fi


if [[ -z $IMAGE ]];then
    for IMAGE in $AVAILABLE_IMAGES
    do
        build_and_push_image $IMAGE_VERSION
    done
else
    build_and_push_image $IMAGE_VERSION
fi
