# Docker images

Contains a collection of Docker images.

Available images:

- [poetry](poetry): basic python image with poetry installed
- [powerhouse](powerhouse): image with numerous shell scripts installed for debugging purpose (network, database, cloud)

Each image directory has a `Dockerfile` as well as a `versions` file with all the available versions for this image.
The `versions` file should be in **descending** order. It means that the latest version should be at the top of the file. This will allow this version to also be tagged as `latest` in the CI.

## Usage

### Locally

Use the script:
`./build.sh [-b] [-r <registry>] <image_name> <version>`

with:

- `-b` will only build the images and not push them
- `registry` is a registry name. Default is [fricounet](https://hub.docker.com/u/fricounet) on docker hub.
- `image_name` is the same name as the one used for the image directories. If not specified, all `Dockerfiles` will be built.
- `version` is the specific version tag to build. If not specified all the versions in the `versions` file will be built.

### CI

There is a CI job available to automatically build and push a selected image:

- go to [the pipeline page](https://gitlab.com/Fricounet/templatoons/-/pipelines/new).
- fill in the relevant variables according to your needs
- wait for the job to succeed and the image will be released to the correct registry
