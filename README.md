[![Latest Release](https://gitlab.com/Fricounet/templatoons/-/badges/release.svg)](https://gitlab.com/Fricounet/templatoons/-/releases)  [![pipeline status](https://gitlab.com/Fricounet/templatoons/badges/main/pipeline.svg)](https://gitlab.com/Fricounet/templatoons/-/commits/main)

# Templatoons

Collection of helpers used to bootstrap projects. Contains gitlab-ci templates, language-specific project bootstraps and common docker-images.

## Usage

You can refer to the READMEs of each directories:

- [Docker images](./docker-images/README.md)
- [Gitlab-ci](./gitlab-ci/README.md)
- [Python](./python/README.md)

## Author

[Baptiste G-C](mailto:baptiste.gc@fricou.net)
