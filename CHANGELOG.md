# Changelog

All notable changes to `templatoons` will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Docker images

#### Added

- Release version 3.11 of `poetry` image

## [v0.3.0] - 2023-03-15

### Docker images

#### Changed

- Packages in `powerhouse` image are not locked anymore as it made the build brittle because old versions are not kept in the repositories

### Gitlab-ci

#### Added

- Add interrogate, bandit and isort to poetry CI.

#### Changed

- Fix poetry safety check by ignoring a rule that is not fixed yet in latest versions

### Python

#### Added

- Add interrogate and bandit to the available tools. Used in pre-commit and CI
- Add `.editorconfig` file to have consistent files formatting accross multiple editors

#### Changed

- Flake8 linting will now also use `flake8-bugbear`

### Removed

- Safety pre-commit because it takes too much time to run

## [v0.2.0] - 2023-02-25

### Gitlab-ci

#### Added

- Automate doc generation in the README for each template with a bash script. [@Fricounet](https://github.com/Fricounet). Use the script in the CI.
- CI job to test and release docker images

### Python

### Added

- Add isort to the available tools. Used in pre-commit

### Changed

- Use `flake8-pyproject` instead of `pyproject-flake8` for flake8 config
- Move dev dependencies in `group.dev.dependencies` block in `pyproject.toml`

#### Removed

- Bootstrap script does not init git anymore as it is a mess in monorepos.

### Docker images

#### Added

- Use hadolint to lint dockerfiles
- Bash script to build and push images

## [v0.1.1] - 2022-06-29

### Gitlab-ci

#### Changed

- Poetry template checks must now be extended using `extends`. Will allow a better usage of the templates in repos where the Python code is not at the root of the repo (monorepos).

### Fixed

- `POETRY_VIRTUALENV_IN_PROJECT` was using a bool but a bool is not accepted by gitlab-ci. Now uses a string instead.

### Python

#### Changed

- Lock templatoons version in gitlab-ci.yaml.
- Python checks now extends the poetry template.

## [v0.1.0] - 2022-06-24

### Docker images

#### Added

- Create poetry image for python3.10. It is a basic python image that contains poetry. [@Fricounet](https://github.com/Fricounet)
- Create powerhouse image which is an image containing various cli tools to help debugging containers issues.

### Gitlab-ci

#### Added

- Create a template to create a Giltab release with the content of CHANGELOG.md on tag creation.
- Create a template to test a python application using poetry.
- Document both templates usage

### Python

#### Added

- Bootstrap project using poetry and add basic Python modules useful in development.
- Use `pyproject-flake8` to support flake8 config in pyproject.toml.
- Create `python-bootstrap` script to generate a new project automatically.
- Instructions in the README on how to generate a python project
